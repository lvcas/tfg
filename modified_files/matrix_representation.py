
import matplotlib.pyplot as plt
import numpy as np

# The matrix dimensions have to be [[A_val1, A_val2, A_val3, ..., B_val1, B_val2, B_val3, ...]]
# So, make sure the matrix starts only with two square brackets (and ends with two square brackets too) and that you substitute all ',' by ','.


a = np.array()

# Display matrix
plt.matshow(a) # With colours
plt.matshow(a, cmap=plt.cm.gray) # Grey scale

plt.show()
