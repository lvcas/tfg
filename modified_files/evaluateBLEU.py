import nltk

# Usage: Place a file with reference sentenes and another with hypothetic translations in the same folder as this file.
#        Make sure their names are the same as stated below (referenceFilename and hypothesisFilename).
#        Execute the file withput parameters.


referenceFilename = 'reference.txt'
hypothesisFilename = 'hypothesis.txt'

references = [line.rstrip('\n') for line in open(referenceFilename)]
hypothesis = [line.rstrip('\n') for line in open(hypothesisFilename)]

numLines = len(references)

if(numLines == len(hypothesis)):
    i = 0
    while i < len(references):
        references[i] = [references[i].split( )]
        hypothesis[i] = hypothesis[i].split( )
        i += 1
    print(references)
    print(hypothesis)

    BLEUscore = nltk.translate.bleu_score.corpus_bleu(references, hypothesis)
    print(BLEUscore)

else:
    print("'"+referenceFilename+"' and '"+hypothesisFilename+"' don't have the same amount of sentences.")
