

#import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
#vector_size = 14 # number of words in sentence (catalan)
#main_word = 5 # index of i word

a = 1.0 # bell height
b = 0.0 # peak position
c = 0.99 # bell width
def gauss(a, b, c, x):
    return a*np.exp(-np.power(x-b, 2.) / (2*np.power(c, 2.)))

def tf_gauss(a, b, c, x):
    #return a*np.exp(-np.power(x-b, 2.) / (2*np.power(c, 2.)))
    #return tf.multiply(a, tf.exp(tf.multiply(-1,tf.pow(tf.subtract(x, b), 2))) / (tf.multiply(2, tf.pow(c, 2))))

    #numerador = tf.multiply(tf.constant(-1.0, dtype=tf.float32),tf.pow(x-tf.constant(b), tf.constant(2.0)))
    base = tf.subtract(tf.cast(x, tf.float32),tf.constant(b))
    multiplicador = tf.pow(base, tf.constant(2.0))
    numerador = tf.constant(-1.0, dtype=tf.float32)*multiplicador
    denominador = tf.multiply(2.0, tf.pow(c, tf.constant(2.0)))
    return tf.multiply(a, tf.exp(tf.cast(numerador / denominador, tf.float32)))


def createVector(vector_size, main_word):
    result = []
    finalResult = []
    total = 0
    for i in range(vector_size):
        tmp = gauss(a, b, c, i - main_word)
        total += tmp
        result.append(tmp)


    for i in range(vector_size):
        if(total == 0):
            break
        result[i] = result[i]/total

    finalResult.append(result)
    return finalResult




def cond(vector_size, output, i, main_word, total):
    return tf.less(i, vector_size)


def cond2(vector_size, finalResult, output, i, total):
    return tf.less(i, vector_size)


def body(vector_size, output2, i, main_word, total):
    tmp = tf_gauss(a, b, c, tf.subtract(i, main_word))
    output2 = output2.write(i, tmp)
    return vector_size, output2, i+1, main_word, total+tmp


def body2(vector_size, finalResult, output, i, total):
    finalResult = finalResult.write(i, output[i]/total)
    return vector_size, finalResult, output, i+1, total




def tf_createVector(vector_size, main_word):

    result = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
    finalResult = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
    total = tf.constant(0.0)


    _, output1, _, _, total = tf.while_loop(cond, body, [vector_size, result, 0, main_word, total])

    aux = output1.stack()

    _, output2, _, _, _ = tf.while_loop(cond2, body2, [vector_size, finalResult, aux, 0, total])

    return [output2.stack()]


def main():
    print(createVector(9, 4))

if __name__ == "__main__":
    main()
